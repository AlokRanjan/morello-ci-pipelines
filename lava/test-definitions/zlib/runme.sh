#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo system.tar.xz "${SYSTEM_URL}"
tar -xf system.tar.xz

adb shell mkdir -p /data/local/tmp/system/bin

for BUILD_SUFFIX in "" "-c64"; do
  adb push system/bin/zlib_bench${BUILD_SUFFIX} /data/local/tmp/system/bin
  adb push system/bin/minigzip${BUILD_SUFFIX} /data/local/tmp/system/bin
done

for BUILD_SUFFIX in "64" "c64"; do
  adb shell mkdir -p /data/local/tmp/system/lib${BUILD_SUFFIX}
  adb push system/lib${BUILD_SUFFIX}/libz.so /data/local/tmp/system/lib${BUILD_SUFFIX}
done

echo 'set -ex
cd /data/local/tmp

BIN_SUFFIX[0]=""
BIN_SUFFIX[1]="-c64"
LIB_SUFFIX[0]="64"
LIB_SUFFIX[1]="c64"

for BUILD_SUFFIX_INDEX in 0 1; do
  BUILD_SUFFIX_BIN=${BIN_SUFFIX[${BUILD_SUFFIX_INDEX}]}
  BUILD_SUFFIX_LIB=${LIB_SUFFIX[${BUILD_SUFFIX_INDEX}]}

  LD_LIBRARY_PATH=/data/local/tmp/system/lib${BUILD_SUFFIX_LIB} \
    /data/local/tmp/system/bin/zlib_bench${BUILD_SUFFIX_BIN} zlib --compression 1 \
    /data/local/tmp/system/bin/zlib_bench${BUILD_SUFFIX_BIN}

  LD_LIBRARY_PATH=/data/local/tmp/system/lib${BUILD_SUFFIX_LIB} \
    /data/local/tmp/system/bin/minigzip${BUILD_SUFFIX_BIN} \
      < /data/local/tmp/system/bin/minigzip${BUILD_SUFFIX_BIN} \
      > temp.gz

  LD_LIBRARY_PATH=/data/local/tmp/system/lib${BUILD_SUFFIX_LIB} \
    /data/local/tmp/system/bin/minigzip${BUILD_SUFFIX_BIN} -d \
      < temp.gz \
      > temp

  diff temp /data/local/tmp/system/bin/minigzip${BUILD_SUFFIX_BIN}
done
' > target-script-zlib.sh
adb push target-script-zlib.sh /data/local/tmp/
adb shell "sh /data/local/tmp/target-script-zlib.sh" > /tmp/zlib-log.txt 2>&1
result=$?
cat /tmp/zlib-log.txt
if [ "$result" = "0" ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=zlib RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=zlib RESULT=fail>"
fi

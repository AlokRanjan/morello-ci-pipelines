#!/bin/sh

set +x

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo pdfium-testfiles.tar.xz "${PDFIUM_URL}"
curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo system.tar.xz "${SYSTEM_URL}"
tar -xf pdfium-testfiles.tar.xz
tar -xf system.tar.xz

adb shell mkdir -p /data/local/tmp/system/bin
adb shell mkdir -p /data/local/tmp/pdfium-testfiles

for BUILD_SUFFIX in "" "-c64"; do
  adb push system/bin/pdfium_test${BUILD_SUFFIX} /data/local/tmp/system/bin
done

for BUILD_SUFFIX in "64" "c64"; do
  adb shell mkdir -p /data/local/tmp/system/lib${BUILD_SUFFIX}

  adb push system/lib${BUILD_SUFFIX}/libft2.so /data/local/tmp/system/lib${BUILD_SUFFIX}
  adb push system/lib${BUILD_SUFFIX}/libjpeg.so /data/local/tmp/system/lib${BUILD_SUFFIX}
  adb push system/lib${BUILD_SUFFIX}/libpng.so /data/local/tmp/system/lib${BUILD_SUFFIX}
  adb push system/lib${BUILD_SUFFIX}/libz.so /data/local/tmp/system/lib${BUILD_SUFFIX}
done

adb push pdfium-testfiles/*.pdf /data/local/tmp/pdfium-testfiles

echo 'set -ex
cd /data/local/tmp/pdfium-testfiles
BIN_SUFFIX[0]=""
BIN_SUFFIX[1]="-c64"
LIB_SUFFIX[0]="64"
LIB_SUFFIX[1]="c64"

for BUILD_SUFFIX_INDEX in 0 1; do
  BUILD_SUFFIX_BIN=${BIN_SUFFIX[${BUILD_SUFFIX_INDEX}]}
  BUILD_SUFFIX_LIB=${LIB_SUFFIX[${BUILD_SUFFIX_INDEX}]}

  LD_LIBRARY_PATH=/data/local/tmp/system/lib${BUILD_SUFFIX_LIB} \
    /data/local/tmp/system/bin/pdfium_test${BUILD_SUFFIX_BIN} page_labels.pdf
done
' > target-script-pdfium.sh
adb push target-script-pdfium.sh /data/local/tmp/
adb shell "sh /data/local/tmp/target-script-pdfium.sh" > /tmp/pdfium-log.txt 2>&1
result=$?
cat /tmp/pdfium-log.txt
if [ "$result" = "0" ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=pdfium RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=pdfium RESULT=fail>"
fi

#!/bin/sh

set +x

dmesg | grep -q "SMP: Total of 4 processors activated"
status=$?
echo "<LAVA_SIGNAL_STARTTC multicore-boot-dmesg>"
if [ "$status" -eq 0 ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=multicore-boot-dmesg RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=multicore-boot-dmesg RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC multicore-boot-dmesg>"

processors=$(nproc)
echo "<LAVA_SIGNAL_STARTTC multicore-boot-nproc>"
if [ $processors -eq 4 ]; then
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=multicore-boot-nproc RESULT=pass>"
else
  echo "<LAVA_SIGNAL_TESTCASE TEST_CASE_ID=multicore-boot-nproc RESULT=fail>"
fi
echo "<LAVA_SIGNAL_ENDTC multicore-boot-nproc>"

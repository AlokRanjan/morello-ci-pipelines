#!/usr/bin/env python3

import argparse
import logging
import os
import requests
import sys
import threading
import time
import yaml
from configobj import ConfigObj, ConfigObjError
from jinja2 import (
    Environment,
    FileSystemLoader,
    StrictUndefined,
)
from urllib.parse import urlsplit, urljoin


FORMAT = "[%(funcName)16s() ] %(message)s"
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger(__name__)


if sys.stdout.isatty():
    COLORS = {
        "exception": "\033[1;31m",
        "error": "\033[1;31m",
        "fail": "\033[1;31m",
        "warning": "\033[1;33m",
        "info": "\033[1;37m",
        "pass": "\033[1;37m",
        "debug": "\033[0;37m",
        "target": "\033[32m",
        "input": "\033[0;35m",
        "skip": "\033[0;35m",
        "unknown": "\033[0;35m",
        "feedback": "\033[0;33m",
        "results": "\033[1;34m",
        "dt": "\033[0;90m",
        "end": "\033[0m",
    }
else:
    COLORS = {
        "exception": "",
        "error": "",
        "warning": "",
        "info": "",
        "debug": "",
        "target": "",
        "input": "",
        "feedback": "",
        "results": "",
        "dt": "",
        "end": "",
        "pass": "",
        "fail": "",
        "skip": "",
        "unknown": "",
    }


def print_u(string):
    try:
        print(string)
    except UnicodeEncodeError:
        print(string.encode("ascii", errors="replace").decode("ascii"))


class NullAuth(requests.auth.AuthBase):
    """force requests to ignore the ``.netrc``

    Some sites do not support regular authentication, but we still
    want to store credentials in the ``.netrc`` file and submit them
    as form elements. Without this, requests would otherwise use the
    .netrc which leads, on some sites, to a 401 error.

    Use with::

        requests.get(url, auth=NullAuth())
    """

    def __call__(self, r):
        return r


class LavaAPI(object):
    def __init__(self, args):
        self.lava_server = args.lava_server
        if not (
            self.lava_server.startswith("http://")
            or self.lava_server.startswith("https://")
        ):
            self.lava_server = "https://" + self.lava_server
        self.lava_url_base = "%s://%s/api/v0.2/" % (
            urlsplit(self.lava_server).scheme,
            urlsplit(self.lava_server).netloc,
        )
        self.lava_auth_dict = {"Authorization": "Token %s" % args.lava_token}

    def get(self, path, params={}):
        return requests.get(
            urljoin(self.lava_url_base, path),
            headers=self.lava_auth_dict,
            params=params,
            auth=NullAuth(),
        )

    def post(self, path, data):
        return requests.post(
            urljoin(self.lava_url_base, path),
            headers=self.lava_auth_dict,
            data=data,
            auth=NullAuth(),
        )


class JobListener(threading.Thread):
    def __init__(self, job_id, lava_api):
        self.job_id = job_id
        self.lava_api = lava_api
        self._return = 0
        threading.Thread.__init__(self)

    def run(self):
        exit = False
        log_start = 0
        log_end = None
        job_status = None
        while not exit:
            # get job status
            old_job_status = job_status
            job_details_request = self.lava_api.get("jobs/%s/" % self.job_id)
            if job_details_request.status_code == 200:
                job_details = job_details_request.json()
                job_status = job_details["state"]
                if old_job_status != job_status:
                    logger.info("Job %s status: %s" % (self.job_id, job_status))
                if job_details["state"] == "Finished":
                    logger.info("Job finished as %s" % job_details["health"])
                    exit = True
                    if job_details["health"] != "Complete":
                        self._return = 1

            # get and print job logs
            log_params = {"start": log_start}
            if log_end is not None:
                log_params.update({"end": log_end})
            job_logs_request = self.lava_api.get(
                "jobs/%s/logs" % self.job_id, params=log_params
            )
            if job_logs_request.status_code == 200:
                logger.debug("Received logs")
                log_list = yaml.load(job_logs_request.content, Loader=yaml.SafeLoader)
                log_start = log_start + len(log_list)
                for line in log_list:
                    timestamp = line["dt"].split(".")[0]
                    level = line["lvl"]
                    if (
                        isinstance(line["msg"], dict)
                        and "sending" in line["msg"].keys()
                    ):
                        level = "input"
                        msg = str(line["msg"]["sending"])
                    elif isinstance(line["msg"], bytes):
                        msg = line["msg"].decode("utf-8", errors="replace")
                    else:
                        msg = str(line["msg"])
                    msg = msg.rstrip("\n")

                    print_u(
                        "Job[{}] ".format(self.job_id)
                        + COLORS["dt"]
                        + timestamp
                        + COLORS["end"]
                        + " "
                        + COLORS[level]
                        + msg
                        + COLORS["end"]
                    )
            time.sleep(5)

        # download results and pass to gitlab
        next_url = "jobs/%s/tests/" % self.job_id
        while next_url is not None:
            test_results_request = self.lava_api.get(next_url)
            if test_results_request.status_code == 200:
                test_results_response = test_results_request.json()
                test_results = test_results_response["results"]
                next_url = test_results_response["next"]
                for test_result in test_results:
                    print_u(
                        "Job[{}] ".format(self.job_id)
                        + COLORS[test_result["result"]]
                        + "%s: %s" % (test_result["name"], test_result["result"])
                        + COLORS["end"]
                    )
                    # add a workaround for LAVA falsly reporting fail
                    # on successful downloads
                    if (
                        test_result["result"] == "fail"
                        and test_result["name"] != "http-download"
                    ):
                        self._return += 1

        # report a LAVA UI URL for easier access:
        print_u("%s/scheduler/job/%s" % (self.lava_api.lava_server, self.job_id))

    def join(self):
        threading.Thread.join(self)
        return self._return


def main():
    parser = argparse.ArgumentParser()
    # qa-reports parameters
    parser.add_argument(
        "--environment",
        help="User specified the environment name, prefix or suffix won't be used",
        dest="environment",
        default="",
    )
    parser.add_argument(
        "--build-id", "--build-number", help="ID for the build", dest="build_id"
    )
    # rendering parameters (test plan)
    parser.add_argument(
        "--variables",
        help="Path to file(s) with variable values",
        dest="variables",
        nargs="+",
        default=[],
    )
    parser.add_argument(
        "--overwrite-variables",
        help="Key-value pairs overwriting variables from the file",
        nargs="+",
        dest="overwrite_variables",
        default=[],
    )
    parser.add_argument(
        "--template-path",
        help="Path to LAVA job templates",
        dest="template_path",
        default="lava_templates",
    )
    parser.add_argument(
        "--template-name",
        help="Name of the LAVA job template",
        dest="template_name",
        required=True,
    )
    # lava parameters
    parser.add_argument(
        "--lava-server",
        help="LAVA server URL",
        dest="lava_server",
        required=True,
    )
    parser.add_argument(
        "--lava-token",
        help="LAVA authentication token",
        dest="lava_token",
        default=os.environ.get("LAVA_TOKEN"),
    )
    parser.add_argument(
        "--verbose",
        help="""Verbosity level. Follows logging levels:
                          CRITICAL: 50
                          ERROR: 40
                          WARNING: 30
                          INFO: 20
                          DEBUG: 10
                          NOTSET: 0""",
        dest="verbose",
        type=int,
        default=logging.INFO,
    )

    args = parser.parse_args()

    logger.setLevel(args.verbose)

    if not args.lava_token:
        logger.error("LAVA token not available")
        sys.exit(1)

    logger.debug(args.lava_token)
    THIS_DIR = os.path.abspath(args.template_path)

    j2_env = Environment(
        loader=FileSystemLoader(THIS_DIR, followlinks=True), undefined=StrictUndefined
    )

    context = {}
    context.update(os.environ)
    for variables in args.variables:
        try:
            context.update(ConfigObj(variables).dict())
        except ConfigObjError:
            logger.info("Unable to parse .ini file")
            logger.info("Trying YAML")
            with open(variables, "r") as vars_file:
                try:
                    context.update(yaml.load(vars_file, Loader=yaml.SafeLoader))
                except ParserError as e:
                    logger.error(e)
                except ComposerError as e:
                    logger.error(e)

    for variable in args.overwrite_variables:
        key, value = variable.split("=")
        context.update({key: value})

    lava_job = j2_env.get_template(args.template_name).render(context)
    logger.info(lava_job)

    skip_lava = os.environ.get('SKIP_LAVA')
    if skip_lava is None:
        lava_api = LavaAPI(args)
        # submit lava job
        lava_create_job_request = lava_api.post("jobs/", {"definition": lava_job})
        exit_status = 0
        if lava_create_job_request.status_code == 201:
            # success
            job_ids = lava_create_job_request.json()["job_ids"]
            # start job listeners
            listeners = []
            for job_id in job_ids:
                job_listener = JobListener(job_id, lava_api)
                listeners.append(job_listener)
                job_listener.start()
            for job_listener in listeners:
                exit_status = exit_status + job_listener.join()
        else:
            logger.error(lava_create_job_request.status_code)
            logger.error(lava_create_job_request.json())
            sys.exit(1)
        sys.exit(exit_status)
    else:
        print_u("LAVA job submission skipped.")
        sys.exit()

if __name__ == "__main__":
    main()

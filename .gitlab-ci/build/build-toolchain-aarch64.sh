# This script builds Morello LLVM toolchain from scratch.
# This includes Clang, Musl libc, CRT objects and compiler-rt.
# Musl libc is built with libshim.
# Cmake should be on PATH.

set -e
set -x

# **** Parameters ****
# host LLVM
HOST_LLVM_PATH=${1:-/usr/local/llvm}
# path to LLVM sources:
LLVM_PROJECT=${2:-llvm-project}
# path to musl libc source
MUSL_PROJECT=${3:-musl-libc}
# path to libshim (libarchcap must be located next to this path)
SHIM_PROJECT=${4:-libshim}
# target path where toolchain will be installed
MORELLO_HOME=${5:-llvm-aarch64-morello-linux}
# workspace (where build files will be placed)
WORKSPACE=${6:-${HOME}}

CURDIR=$(pwd)
LLVM_LIT_ARGS='-s --no-progress-bar --workers=20 --xunit-xml-output llvm-test-results.xml'

# Step 1: Configure Clang
HOST_LLVM_BIN=${HOST_LLVM_PATH}/bin
TARGETS=AArch64
rm -rf ${MORELLO_HOME}
mkdir -p ${MORELLO_HOME}
mkdir -p ${WORKSPACE}/build
cd ${WORKSPACE}/build
cmake \
    -DCMAKE_C_COMPILER=${HOST_LLVM_BIN}/clang \
    -DCMAKE_C_COMPILER_WORKS=YES \
    -DCMAKE_CXX_COMPILER=${HOST_LLVM_BIN}/clang++ \
    -DCMAKE_CXX_COMPILER_WORKS=YES \
    -DCMAKE_AR=${HOST_LLVM_BIN}/llvm-ar \
    -DCMAKE_RANLIB=${HOST_LLVM_BIN}/llvm-ranlib \
    -DCMAKE_NM=${HOST_LLVM_BIN}/llvm-nm \
    -DCMAKE_LINKER=${HOST_LLVM_BIN}/ld.lld \
    -DCMAKE_OBJDUMP=${HOST_LLVM_BIN}/llvm-objdump \
    -DCMAKE_OBJCOPY=${HOST_LLVM_BIN}/llvm-objcopy \
    -DCMAKE_EXE_LINKER_FLAGS="-fuse-ld=lld" \
    -DCMAKE_SHARED_LINKER_FLAGS="-fuse-ld=lld" \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=${MORELLO_HOME} \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DCMAKE_SKIP_BUILD_RPATH=OFF \
    -DCMAKE_INSTALL_RPATH=\$ORIGIN/../lib \
    -DCMAKE_BUILD_WITH_INSTALL_RPATH=ON \
    -DLLVM_ENABLE_PROJECTS="clang;lld;lldb;libcxx;libcxxabi;compiler-rt;libunwind" \
    -DLLVM_TARGETS_TO_BUILD="${TARGETS}" \
    -DLLVM_ENABLE_ASSERTIONS=OFF \
    -DLLVM_ENABLE_LIBCXX=ON \
    -DLLVM_ENABLE_LLD=ON \
    -DLLVM_ENABLE_EH=ON \
    -DLLVM_ENABLE_RTTI=ON \
    -DBUILD_SHARED_LIBS=ON \
    -DCOMPILER_RT_BUILD_BUILTINS=ON \
    -DCOMPILER_RT_BUILD_XRAY=OFF \
    -DCOMPILER_RT_BUILD_LIBFUZZER=OFF \
    -DCOMPILER_RT_BUILD_PROFILE=OFF \
    -DLIBCXX_CXX_ABI=libcxxabi \
    -DLIBCXX_CXX_ABI_INCLUDE_PATHS="${LLVM_PROJECT}/libcxxabi/include" \
    -DLIBCXX_USE_COMPILER_RT=ON \
    -DLIBCXX_ENABLE_THREADS=ON \
    -DLIBCXXABI_ENABLE_THREADS=ON \
    -DLIBCXXABI_USE_LLVM_UNWINDER=ON \
    -DLIBCXXABI_USE_COMPILER_RT=ON \
    -DLIBCXXABI_USE_LLVM_UNWINDER=ON \
    -DLIBUNWIND_ENABLE_THREADS=ON \
    -DLLVM_LIT_ARGS="${LLVM_LIT_ARGS}" \
    -DCLANG_DEFAULT_RTLIB="compiler-rt" \
    -DCLANG_DEFAULT_CXX_STDLIB="libc++" \
    -DCLANG_DEFAULT_LINKER="lld" \
    -DCLANG_DEFAULT_OBJCOPY="llvm-objcopy" \
    ${LLVM_PROJECT}/llvm
cd ${CURDIR}

# Step 2: Build Clang
cd ${WORKSPACE}/build
make -j20
make install
cd ${CURDIR}

# Step 3: Run Clang Unit tests
cd ${WORKSPACE}/build
make -j20 UnitTests
LD_LIBRARY_PATH=${WORKSPACE}/build/lib make check-llvm
cd ${CURDIR}

# Step 4: Build Musl
export CC=${MORELLO_HOME}/bin/clang
TRIPLE=aarch64-linux-musl_purecap
mkdir -p ${WORKSPACE}/build-musl
cd ${WORKSPACE}/build-musl
CC=${MORELLO_HOME}/bin/clang ${MUSL_PROJECT}/configure --prefix=${MORELLO_HOME}/sysroot --disable-shared --enable-morello --enable-libshim --libshim-path=${SHIM_PROJECT} --target=${TRIPLE}
make -j8
make install
cd ${CURDIR}

# Step 5: Build CRT objects
export CC=${MORELLO_HOME}/bin/clang
EXPANDED_TRIPLE=aarch64-unknown-linux-musl_purecap
DESTDIR=$(${CC} -print-resource-dir)/lib/${EXPANDED_TRIPLE}
mkdir -p ${DESTDIR}
${CC} -march=morello+c64 -mabi=purecap -nostdinc -isystem ${MORELLO_HOME}/sysroot/include -c ${LLVM_PROJECT}/compiler-rt/lib/crt/crtbegin.c -o ${DESTDIR}/clang_rt.crtbegin.o
${CC} -march=morello+c64 -mabi=purecap -nostdinc -isystem ${MORELLO_HOME}/sysroot/include -c ${LLVM_PROJECT}/compiler-rt/lib/crt/crtend.c -o ${DESTDIR}/clang_rt.crtend.o

# Copy CRT files
cp ${DESTDIR}/clang_rt.crtbegin.o ${WORKSPACE}
cp ${DESTDIR}/clang_rt.crtend.o ${WORKSPACE}

# Step 6: Build compiler-rt
EXPANDED_TRIPLE=aarch64-unknown-linux-musl_purecap
DESTDIR=$(${CC} -print-resource-dir)/lib/${EXPANDED_TRIPLE}
mkdir -p ${WORKSPACE}/build-rt
cd ${WORKSPACE}/build-rt
cat > toolchain.cmake <<EOF
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)
set(CMAKE_C_COMPILER_TARGET "aarch64-linux-gnu -march=morello+c64 -mabi=purecap")

set(CMAKE_C_COMPILER_WORKS 1 CACHE INTERNAL "")
set(CMAKE_CXX_COMPILER_WORKS 1 CACHE INTERNAL "")

set(CMAKE_C_COMPILER "${MORELLO_HOME}/bin/clang" CACHE FILEPATH "" FORCE)
set(CMAKE_CXX_COMPILER "${MORELLO_HOME}/bin/clang++" CACHE FILEPATH "" FORCE)
set(CMAKE_AR "${MORELLO_HOME}/bin/llvm-ar" CACHE FILEPATH "" FORCE)
set(CMAKE_RANLIB "${MORELLO_HOME}/bin/llvm-ranlib" CACHE FILEPATH "" FORCE)
set(CMAKE_NM "${MORELLO_HOME}/bin/llvm-nm" CACHE FILEPATH "" FORCE)
set(CMAKE_LINKER "${MORELLO_HOME}/bin/ld.lld" CACHE FILEPATH "" FORCE)
set(CMAKE_OBJDUMP "${MORELLO_HOME}/bin/llvm-objdump" CACHE FILEPATH "" FORCE)
set(CMAKE_OBJCOPY "${MORELLO_HOME}/bin/llvm-objcopy" CACHE FILEPATH "" FORCE)

set(LLVM_CONFIG_PATH "${MORELLO_HOME}/bin/llvm-config" CACHE FILEPATH "" FORCE)
set(CMAKE_EXE_LINKER_FLAGS "-fuse-ld=lld" CACHE FILEPATH "" FORCE)
set(CMAKE_SHARED_LINKER_FLAGS "-fuse-ld=lld" CACHE FILEPATH "" FORCE)
EOF
cmake -Wno-dev \
    -DCMAKE_TOOLCHAIN_FILE=toolchain.cmake \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_C_FLAGS="-nostdinc -isystem ${MORELLO_HOME}/sysroot/include" \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DCMAKE_SKIP_BUILD_RPATH=OFF \
    -DCMAKE_INSTALL_RPATH=\$ORIGIN/../lib \
    -DCMAKE_BUILD_WITH_INSTALL_RPATH=ON \
    -DLLVM_TARGETS_TO_BUILD="AArch64" \
    -DLLVM_ENABLE_ASSERTIONS=OFF \
    -DBUILD_SHARED_LIBS=ON \
    -DCOMPILER_RT_DEFAULT_TARGET_TRIPLE=aarch64-linux-gnu \
    -DCOMPILER_RT_BUILD_BUILTINS=ON \
    -DCOMPILER_RT_BUILD_SANITIZERS=OFF \
    -DCOMPILER_RT_BUILD_XRAY=OFF \
    -DCOMPILER_RT_BUILD_LIBFUZZER=OFF \
    -DCOMPILER_RT_BUILD_PROFILE=OFF \
    ${LLVM_PROJECT}/compiler-rt
make -j4 clang_rt.builtins-aarch64
cp lib/linux/libclang_rt.builtins-aarch64.a ${DESTDIR}/libclang_rt.builtins.a
cp ${DESTDIR}/libclang_rt.builtins.a ${WORKSPACE}/libclang_rt.builtins.a

cd ${CURDIR}

# Step 7: Build and run Musl tests (requires Morello IE)
cd ${MUSL_PROJECT}
export MORELLOIE=$(which morelloie)
export MORELLO_HOME
CC=${MORELLO_HOME}/bin/clang ./configure \
    --disable-shared --enable-morello --enable-libshim --libshim-path=${SHIM_PROJECT} --prefix=${MORELLO_HOME}/sysroot
make -C test test
cd ${CURDIR}

# Step 8: Package artefacts
BASEPATH=$(dirname ${MORELLO_HOME})
BASENAME=$(basename ${MORELLO_HOME})
EXPANDED_TRIPLE=aarch64-linux-musl_purecap
DESTDIR=$(${CC} -print-resource-dir)/lib/${EXPANDED_TRIPLE}

mkdir -p ${MORELLO_HOME}/thirdpartylicences
cat > ${MORELLO_HOME}/NOTICE.txt <<EOF
This product embeds and uses the following pieces of software which have
additional or alternate licenses:
 - LLVM: thirdpartylicences/LLVM-LICENSE.TXT
 - Clang: thirdpartylicences/CLANG-LICENSE.TXT
 - lldb: thirdpartylicences/LLDB-LICENSE.TXT
 - lld: thirdpartylicences/LLD-LICENSE.TXT
 - libc++: thirdpartylicences/LIBCXX-LICENSE.TXT
 - libc++abi: thirdpartylicences/LIBCXXABI-LICENSE.TXT
 - libunwind: thirdpartylicences/LIBUNWIND-LICENSE.TXT
 - libclc: thirdpartylicences/LIBCLC-LICENSE.TXT
 - openmp: thirdpartylicences/OPENMP-LICENSE.TXT
 - parallel-libs: thirdpartylicences/PARALLEL-LIBS-ACXXEL-LICENSE.TXT
 - polly: thirdpartylicences/POLLY-LICENSE.TXT
 - pstl: thirdpartylicences/PSTL-LICENSE.TXT
 - clang-tools-extra: thirdpartylicences/CLANG-TOOLS-EXTRA-LICENSE.TXT
 - compiler-rt: thirdpartylicences/COMPILER-RT-LICENSE.TXT

EOF

cp ${LLVM_PROJECT}/llvm/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LLVM-LICENSE.TXT
cp ${LLVM_PROJECT}/clang/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/CLANG-LICENSE.TXT
cp ${LLVM_PROJECT}/lldb/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LLDB-LICENSE.TXT
cp ${LLVM_PROJECT}/lld/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LLD-LICENSE.TXT
cp ${LLVM_PROJECT}/libcxx/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LIBCXX-LICENSE.TXT
cp ${LLVM_PROJECT}/libcxxabi/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LIBCXXABI-LICENSE.TXT
cp ${LLVM_PROJECT}/libunwind/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LIBUNWIND-LICENSE.TXT
cp ${LLVM_PROJECT}/compiler-rt/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/COMPILER-RT-LICENSE.TXT
cp ${LLVM_PROJECT}/libclc/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LIBCLC-LICENSE.TXT
cp ${LLVM_PROJECT}/openmp/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/OPENMP-LICENSE.TXT
cp ${LLVM_PROJECT}/parallel-libs/acxxel/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/PARALLEL-LIBS-ACXXEL-LICENSE.TXT
cp ${LLVM_PROJECT}/polly/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/POLLY-LICENSE.TXT
cp ${LLVM_PROJECT}/pstl/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/PSTL-LICENSE.TXT
cp ${LLVM_PROJECT}/clang-tools-extra/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/CLANG-TOOLS-EXTRA-LICENSE.TXT

cat > ${MORELLO_HOME}/sysroot/NOTICE.txt <<EOF
This product embeds and uses the following pieces of software which have
additional or alternate licenses:
 - Musl: share/MUSL-LICENSE.TXT
 - Libshim: share/LIBSHIM-LICENSE.TXT
 - Libarchcap: share/LIBARCHCAP-LICENSE.TXT

EOF
wget -q https://www.apache.org/licenses/LICENSE-2.0.txt -O ${MORELLO_HOME}/sysroot/LICENSE.txt
mkdir -p ${MORELLO_HOME}/sysroot/share
cp ${MUSL_PROJECT}/COPYRIGHT ${MORELLO_HOME}/sysroot/share/MUSL-LICENSE.TXT
cp ${SHIM_PROJECT}/LICENSE.txt ${MORELLO_HOME}/sysroot/share/LIBSHIM-LICENSE.TXT
cp ${SHIM_PROJECT}/../libarchcap/LICENSE.txt ${MORELLO_HOME}/sysroot/share/LIBARCHCAP-LICENSE.TXT

cd ${BASEPATH}
rm -vf ${WORKSPACE}/llvm-morello-linux-aarch64-clang.tar.xz
rm -vf ${WORKSPACE}/llvm-morello-linux-aarch64-sysroot.tar.xz
tar -cjf ${WORKSPACE}/llvm-morello-linux-aarch64-sysroot.tar.xz ${BASENAME}/sysroot
rm -rf ${BASENAME}/sysroot
tar -cjf ${WORKSPACE}/llvm-morello-linux-aarch64-clang.tar.xz ${BASENAME}
cd ${CURDIR}

echo "All done"

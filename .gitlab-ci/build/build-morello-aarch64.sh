#!/bin/sh


set -ex
MORELLO_AARCH64_PROJECT_BRANCH="${CI_COMMIT_REF_NAME:-morello/mainline}"

cd ${CI_PROJECT_DIR}
git clone https://git.morello-project.org/morello/morello-aarch64 -b ${MORELLO_AARCH64_PROJECT_BRANCH}
cd morello-aarch64/morello
source ./env/morello-aarch64
if [ $(arch) == x86_64 ]; then
./scripts/build-all.sh --x86_64
else
./scripts/build-all.sh
fi

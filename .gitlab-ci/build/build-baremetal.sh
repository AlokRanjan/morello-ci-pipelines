#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"
MORELLO_TLD="https://git.morello-project.org/morello"
MORELLO_TC_BINARIES_REPO="llvm-project-releases"
BM_TC_RELEASE="morello/baremetal-release-1.4"

set -ex
#clone the repo with baremetal tc releast branch
git clone -b ${BM_TC_RELEASE} --single-branch ${MORELLO_TLD}/${MORELLO_TC_BINARIES_REPO}.git

rm -rf .repo/manifests
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_BRANCH} -g bsp \
  --repo-rev=v2.16

repo selfupdate
repo version
# For convenience, add gnulib and grub to the bsp group
xmlstarlet edit --inplace \
  --update "//project[@name='gnulib']/@groups" \
  --value android,bsp,busybox \
  .repo/manifests/morello-base.xml
xmlstarlet edit --inplace \
  --update "//project[@name='grub']/@groups" \
  --value android,bsp,busybox \
  .repo/manifests/morello-base.xml

time repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

#create the helloworld app files
mkdir ${CI_PROJECT_DIR}/helloworld
cat > ${CI_PROJECT_DIR}/helloworld/helloworld.c <<EOF
#include <stdio.h>

#define PLAT_ARM_BOOT_UART_BASE 0x2A400000

volatile char * ptr = (volatile char*) PLAT_ARM_BOOT_UART_BASE;

int main()
{
  *ptr='h';
  *ptr='e';
  *ptr='l';
  *ptr='l';
  *ptr='o';
  while (1);
  return 0;
}
EOF

cat > ${CI_PROJECT_DIR}/helloworld/link_scripts.ld.S <<EOF
OUTPUT_FORMAT("elf64-littleaarch64")
OUTPUT_ARCH("aarch64")
ENTRY(main)

MEMORY {
  RAM (rwx): ORIGIN = 0xE0000000, LENGTH = 0xE0000000 + 0x200000
}

SECTIONS
{
  . = 0xE0000000;

  ro . : {
    */helloworld.o(.text)
    *(.text*)
    *(.rodata*)
  } >RAM

  .data : {
    *(.data*)
  } >RAM
}
EOF
#Build the standalone app
cd helloworld
${CI_PROJECT_DIR}/${MORELLO_TC_BINARIES_REPO}/bin/clang -target aarch64-none-elf -c helloworld.c -o helloworld.o -O3
${CI_PROJECT_DIR}/${MORELLO_TC_BINARIES_REPO}/bin/ld.lld -o helloworld -T link_scripts.ld.S helloworld.o -s
${CI_PROJECT_DIR}/${MORELLO_TC_BINARIES_REPO}/bin/llvm-objcopy -O binary helloworld
cmake --version
cd -
#Build the firmware
time bash -x ./build-scripts/build-all.sh -p fvp -f none
#Start packaging the app
make -C "bsp/arm-tf" PLAT=morello TARGET_PLATFORM=fvp clean

MBEDTLS_DIR="${CI_PROJECT_DIR}/bsp/deps/mbedtls" \
    CROSS_COMPILE="${CI_PROJECT_DIR}/tools/clang/bin/llvm-" \
    make -C "bsp/arm-tf" \
    CC="${CI_PROJECT_DIR}/tools/clang/bin/clang" \
    LD="${CI_PROJECT_DIR}/tools/clang/bin/ld.lld" \
    PLAT=morello ARCH=aarch64 TARGET_PLATFORM=fvp ENABLE_MORELLO_CAP=1 \
    E=0 TRUSTED_BOARD_BOOT=1 GENERATE_COT=1 ARM_ROTPK_LOCATION="devel_rsa" \
    ROT_KEY="plat/arm/board/common/rotpk/arm_rotprivk_rsa.pem" \
    BL33=${CI_PROJECT_DIR}/helloworld/helloworld \
    all fip

cp output/fvp/firmware/scp_romfw.bin output/fvp/firmware/scp_fw.bin \
    output/fvp/firmware/mcp_romfw.bin output/fvp/firmware/mcp_fw.bin \
    bsp/arm-tf/build/morello/release/bl1.bin bsp/arm-tf/build/morello/release/fip.bin \
    ${CI_PROJECT_DIR}

echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env

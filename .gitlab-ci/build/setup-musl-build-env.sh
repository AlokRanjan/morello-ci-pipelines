#!/bin/sh

sudo apt update -q=2
sudo apt install -q=2 --yes --no-install-recommends wget

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"
CHECK_TESTS="${CHECK_TESTS:-"check-llvm check-clang check-lld check-lldb"}"
MUSL_PROJECT_BRANCH="morello/master"
LIBSHIM_REF="morello/mainline"
LIBSHIM_URL="https://git.morello-project.org/morello/android/platform/external/libshim"
LIBARCHCAP_REF="morello/mainline"
LIBARCHCAP_URL="https://git.morello-project.org/morello/android/platform/external/libarchcap"

if [ -z "${ANDROID_GIT_COOKIE}" ]; then
  printf "INFO: Skip http.cookiefile\n"
else
  printf "android.googlesource.com\tFALSE\t/\tTRUE\t2147483647\to\t${ANDROID_GIT_COOKIE}\n" > ~/.gitcookies
  chmod 0600 ~/.gitcookies
  git config --global http.cookiefile ~/.gitcookies
  printf "INFO: Set http.cookiefile\n"
fi

set -ex

if [ -z "${SKIP_UPDATE}" ]; then
  rm -rf .repo/manifests
  repo init --no-clone-bundle \
    -u https://git.morello-project.org/morello/manifest.git \
    -b ${MANIFEST_BRANCH} -g toolchain-src \
    --repo-rev=v2.16

  repo selfupdate
  repo version

  xmlstarlet edit --inplace \
        --update "//project[@name='llvm-project']/@revision" \
        --value morello/dev \
        .repo/manifests/morello-toolchain.xml

  # Set the llvm-project revision
  case "$CI_PROJECT_PATH" in
    morello/llvm-project)
      LLVM_PROJECT_BRANCH="${LLVM_PROJECT_BRANCH:-morello/dev}"
      TAG=$(curl -s https://git.morello-project.org/api/v4/projects/${CI_PROJECT_ID}/repository/tags/${LLVM_PROJECT_BRANCH}|jq '.name // empty')
      if [ ! -z $TAG ]; then
          LLVM_PROJECT_BRANCH=refs/tags/${LLVM_PROJECT_BRANCH}
      fi
      xmlstarlet edit --inplace \
        --update "//project[@name='llvm-project']/@revision" \
        --value ${LLVM_PROJECT_BRANCH} \
        .repo/manifests/morello-toolchain.xml
    ;;
    morello/musl-libc)
      MUSL_PROJECT_BRANCH="${CI_COMMIT_REF_NAME:-$MUSL_PROJECT_BRANCH}"
      ;;
    morello/android/platform/external/libshim)
      export LIBSHIM_REF="${CI_COMMIT_REF_NAME:-$LIBSHIM_REF}"
      ;;
    morello/android/platform/external/libarchcap)
      export LIBARCHCAP_REF="${CI_COMMIT_REF_NAME:-$LIBARCHCAP_REF}"
      ;;
  esac

  if [ ! -z ${PROJECT_REFS+x} ]; then
    ./.gitlab-ci/utils/patch_manifest_repo.sh ${PROJECT_REFS} .repo/manifests
    ./.gitlab-ci/utils/patch_manifest.sh ${PROJECT_REFS} .repo/manifests/morello-toolchain.xml

    project_details=$(echo ${PROJECT_REFS} | base64 --decode)
    mkfifo tempPipe
    echo $project_details | jq -c '.[]' > tempPipe &
    while IFS= read -r project_detail
    do
	PROJECT_PATH=$(echo $project_detail| jq -r ."project_path")
        PROJECT_BRANCH=$(echo $project_detail| jq -r ."branch")
        PROJECT_NAME=$(echo ${PROJECT_PATH} | cut -d'/' -f2-)
        if [[ "$PROJECT_NAME" == "musl-libc" ]]; then
            MUSL_PROJECT_BRANCH=${PROJECT_BRANCH}
        fi
    done < tempPipe
  fi

  # Avoid to download +12G of prebuilt binaries
  sed -i '/darwin/d' .repo/manifests/morello-toolchain.xml
  sed -i '/mingw32/d' .repo/manifests/morello-toolchain.xml
  sed -i '/windows/d' .repo/manifests/morello-toolchain.xml
  if [ -z "${ANDROID_GIT_COOKIE}" ]; then
    printf "INFO: Skip http.cookiefile\n"
  else
    xmlstarlet edit --inplace  \
      --update "//remote[@name='aosp']/@fetch" \
      --value "https://android.googlesource.com/a/" \
      .repo/manifests/remotes.xml
  fi

  time repo sync -j8 --quiet --no-clone-bundle
  repo manifest -r -o pinned-manifest.xml
  cat pinned-manifest.xml
fi

git clone $LIBSHIM_URL -b $LIBSHIM_REF
git clone $LIBARCHCAP_URL -b $LIBARCHCAP_REF
git clone https://git.morello-project.org/morello/musl-libc -b $MUSL_PROJECT_BRANCH
#git clone $LLVM_PROJECT_URL -b $LLVM_PROJECT_REF

which cmake
cmake --version
which python
python --version
which python3
python3 --version

# path to sources of the Morello toolchain
export LLVM_PROJECT="$(pwd)/toolchain-src/toolchain/llvm-project"
# target installation path for the Morello toolchain
export MORELLO_HOME="${CI_PROJECT_DIR}/llvm-aarch64-morello-linux"
# where host LLVM is installed
export HOST_LLVM_PATH=/usr/lib/llvm-10
export SHIM_PROJECT=${PWD}/libshim
export WORKSPACE=${CI_PROJECT_DIR}
export MUSL_PROJECT=${PWD}/musl-libc

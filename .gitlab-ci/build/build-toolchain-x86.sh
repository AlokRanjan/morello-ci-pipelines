# This script builds Morello LLVM toolchain from scratch.
# This includes Clang, Musl libc, CRT objects and compiler-rt.
# Musl libc is built with libshim.
# Cmake should be on PATH.

set -e
set -x

# **** Parameters ****
# host LLVM
HOST_LLVM_PATH=${1:-/usr/lib/llvm-10}
# path to LLVM sources:
LLVM_PROJECT=${2:-$(realpath -m llvm-project)}
# path to musl libc source
MUSL_PROJECT=${3:-$(realpath -m musl-libc)}
# path to libshim (libarchcap must be located next to this path)
SHIM_PROJECT=${4:-$(realpath -m libshim)}
# target path where toolchain will be installed
MORELLO_HOME=${5:-$(realpath -m llvm-aarch64-morello-linux)}
# workspace (where build files will be placed)
WORKSPACE=${6:-${HOME}}

CURDIR=$(pwd)
LLVM_LIT_ARGS='LLVM_LIT_ARGS=-s --no-progress-bar --workers=20 --xunit-xml-output llvm-test-results.xml'

# Step 1: Configure Clang
HOST_LLVM_BIN=${HOST_LLVM_PATH}/bin
TARGETS='X86;AArch64'
rm -rf ${MORELLO_HOME}
mkdir -p ${MORELLO_HOME}
mkdir -p ${WORKSPACE}/build
cd ${WORKSPACE}/build
cmake -Wno-dev \
    -DCMAKE_C_COMPILER=${HOST_LLVM_BIN}/clang \
    -DCMAKE_C_COMPILER_WORKS=YES \
    -DCMAKE_CXX_COMPILER=${HOST_LLVM_BIN}/clang++ \
    -DCMAKE_CXX_COMPILER_WORKS=YES \
    -DCMAKE_AR=${HOST_LLVM_BIN}/llvm-ar \
    -DCMAKE_RANLIB=${HOST_LLVM_BIN}/llvm-ranlib \
    -DCMAKE_NM=${HOST_LLVM_BIN}/llvm-nm \
    -DCMAKE_LINKER=${HOST_LLVM_BIN}/ld.lld \
    -DCMAKE_OBJDUMP=${HOST_LLVM_BIN}/llvm-objdump \
    -DCMAKE_OBJCOPY=${HOST_LLVM_BIN}/llvm-objcopy \
    -DCMAKE_EXE_LINKER_FLAGS="-fuse-ld=lld" \
    -DCMAKE_SHARED_LINKER_FLAGS="-fuse-ld=lld" \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=${MORELLO_HOME} \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DCMAKE_SKIP_BUILD_RPATH=OFF \
    -DCMAKE_INSTALL_RPATH=\$ORIGIN/../lib \
    -DCMAKE_BUILD_WITH_INSTALL_RPATH=ON \
    -DLLVM_ENABLE_PROJECTS="clang;lld;lldb;libcxx;libcxxabi;compiler-rt;libunwind" \
    -DLLVM_TARGETS_TO_BUILD="${TARGETS}" \
    -DLLVM_ENABLE_ASSERTIONS=OFF \
    -DLLVM_ENABLE_LIBCXX=OFF \
    -DLLVM_ENABLE_LLD=ON \
    -DLLVM_ENABLE_EH=ON \
    -DLLVM_ENABLE_RTTI=ON \
    -DBUILD_SHARED_LIBS=ON \
    -DCOMPILER_RT_BUILD_BUILTINS=ON \
    -DCOMPILER_RT_BUILD_XRAY=OFF \
    -DCOMPILER_RT_BUILD_LIBFUZZER=OFF \
    -DCOMPILER_RT_BUILD_PROFILE=OFF \
    -DLIBCXX_CXX_ABI=libcxxabi \
    -DLIBCXX_CXX_ABI_INCLUDE_PATHS="${LLVM_PROJECT}/libcxxabi/include" \
    -DLIBCXX_USE_COMPILER_RT=ON \
    -DLIBCXX_ENABLE_THREADS=ON \
    -DLIBCXXABI_ENABLE_THREADS=ON \
    -DLIBCXXABI_USE_LLVM_UNWINDER=ON \
    -DLIBCXXABI_USE_COMPILER_RT=ON \
    -DLIBUNWIND_ENABLE_THREADS=ON \
    -DLLVM_LIT_ARGS="${LLVM_LIT_ARGS}" \
    -DCLANG_DEFAULT_RTLIB="compiler-rt" \
    -DCLANG_DEFAULT_CXX_STDLIB="libc++" \
    -DCLANG_DEFAULT_LINKER="lld" \
    -DCLANG_DEFAULT_OBJCOPY="llvm-objcopy" \
    ${LLVM_PROJECT}/llvm
cd ${CURDIR}

# Step 2: Build Clang
cd ${WORKSPACE}/build
make -j20
make install
cd ${CURDIR}

# Step 3: Run Clang Unit tests
#cd ${WORKSPACE}/build
#make -j20 UnitTests
#LD_LIBRARY_PATH=${WORKSPACE}/build/lib make check-llvm
#cd ${CURDIR}

# Step 4: Build Musl
export CC=${MORELLO_HOME}/bin/clang
TRIPLE=aarch64-linux-musl_purecap
EXPANDED_TRIPLE=aarch64-unknown-linux-musl_purecap
mkdir -p ${WORKSPACE}/build-musl
cd ${WORKSPACE}/build-musl
CC=${MORELLO_HOME}/bin/clang ${MUSL_PROJECT}/configure --prefix=${MORELLO_HOME}/sysroot --disable-shared --enable-morello --enable-libshim --libshim-path=${SHIM_PROJECT} --target=${TRIPLE}
make -j8
make install
cd ${CURDIR}

# Step 5: Copy CRT objects and compiler-rt from AArch64 under ${MORELLO_HOME}/lib/clang/13.0.0/lib/${EXPANDED_TRIPLE}/
DESTDIR=$(${CC} -print-resource-dir)/lib/${EXPANDED_TRIPLE}
mkdir -p ${DESTDIR}
cp $WORKSPACE/clang_rt.crtbegin.o ${DESTDIR}/clang_rt.crtbegin.o
cp $WORKSPACE/clang_rt.crtend.o ${DESTDIR}/clang_rt.crtend.o
cp $WORKSPACE/libclang_rt.builtins.a ${DESTDIR}/libclang_rt.builtins.a


# Step 6: Package artefacts
BASEPATH=$(dirname ${MORELLO_HOME})
BASENAME=$(basename ${MORELLO_HOME})

mkdir -p ${MORELLO_HOME}/thirdpartylicences
cat > ${MORELLO_HOME}/NOTICE.txt <<EOF
This product embeds and uses the following pieces of software which have
additional or alternate licenses:
 - LLVM: thirdpartylicences/LLVM-LICENSE.TXT
 - Clang: thirdpartylicences/CLANG-LICENSE.TXT
 - lldb: thirdpartylicences/LLDB-LICENSE.TXT
 - lld: thirdpartylicences/LLD-LICENSE.TXT
 - libc++: thirdpartylicences/LIBCXX-LICENSE.TXT
 - libc++abi: thirdpartylicences/LIBCXXABI-LICENSE.TXT
 - libunwind: thirdpartylicences/LIBUNWIND-LICENSE.TXT
 - libclc: thirdpartylicences/LIBCLC-LICENSE.TXT
 - openmp: thirdpartylicences/OPENMP-LICENSE.TXT
 - parallel-libs: thirdpartylicences/PARALLEL-LIBS-ACXXEL-LICENSE.TXT
 - polly: thirdpartylicences/POLLY-LICENSE.TXT
 - pstl: thirdpartylicences/PSTL-LICENSE.TXT
 - clang-tools-extra: thirdpartylicences/CLANG-TOOLS-EXTRA-LICENSE.TXT
 - compiler-rt: thirdpartylicences/COMPILER-RT-LICENSE.TXT

EOF

cp ${LLVM_PROJECT}/llvm/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LLVM-LICENSE.TXT
cp ${LLVM_PROJECT}/clang/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/CLANG-LICENSE.TXT
cp ${LLVM_PROJECT}/lldb/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LLDB-LICENSE.TXT
cp ${LLVM_PROJECT}/lld/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LLD-LICENSE.TXT
cp ${LLVM_PROJECT}/libcxx/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LIBCXX-LICENSE.TXT
cp ${LLVM_PROJECT}/libcxxabi/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LIBCXXABI-LICENSE.TXT
cp ${LLVM_PROJECT}/libunwind/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LIBUNWIND-LICENSE.TXT
cp ${LLVM_PROJECT}/compiler-rt/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/COMPILER-RT-LICENSE.TXT
cp ${LLVM_PROJECT}/libclc/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/LIBCLC-LICENSE.TXT
cp ${LLVM_PROJECT}/openmp/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/OPENMP-LICENSE.TXT
cp ${LLVM_PROJECT}/parallel-libs/acxxel/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/PARALLEL-LIBS-ACXXEL-LICENSE.TXT
cp ${LLVM_PROJECT}/polly/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/POLLY-LICENSE.TXT
cp ${LLVM_PROJECT}/pstl/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/PSTL-LICENSE.TXT
cp ${LLVM_PROJECT}/clang-tools-extra/LICENSE.TXT ${MORELLO_HOME}/thirdpartylicences/CLANG-TOOLS-EXTRA-LICENSE.TXT

mkdir -p ${MORELLO_HOME}/sysroot/share

cat > ${MORELLO_HOME}/sysroot/NOTICE.txt <<EOF
This product embeds and uses the following pieces of software which have
additional or alternate licenses:
 - Musl: share/MUSL-LICENSE.TXT
 - Libshim: share/LIBSHIM-LICENSE.TXT
 - Libarchcap: share/LIBARCHCAP-LICENSE.TXT

EOF

cp ${MUSL_PROJECT}/COPYRIGHT ${MORELLO_HOME}/sysroot/share/MUSL-LICENSE.TXT
cp ${SHIM_PROJECT}/LICENSE.txt ${MORELLO_HOME}/sysroot/share/LIBSHIM-LICENSE.TXT
cp ${SHIM_PROJECT}/../libarchcap/LICENSE.txt ${MORELLO_HOME}/sysroot/share/LIBARCHCAP-LICENSE.TXT
wget -q https://www.apache.org/licenses/LICENSE-2.0.txt -O ${MORELLO_HOME}/sysroot/LICENSE.txt

cd ${BASEPATH}
rm -vf ${WORKSPACE}/llvm-morello-linux-x86*.tar.xz
tar -cjf ${WORKSPACE}/llvm-morello-linux-x86-sysroot.tar.xz ${BASENAME}/sysroot
rm -rf ${BASENAME}/sysroot
tar -cjf ${WORKSPACE}/llvm-morello-linux-x86-clang.tar.xz ${BASENAME}
cd ${CURDIR}

echo "All done"

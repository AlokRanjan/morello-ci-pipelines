#!/bin/sh

source .gitlab-ci/build/setup-android-build-env.sh

if ! [ -z ${PLATFORM+x} ]; then
	time bash -x ./build-scripts/fetch-tools.sh -f android-swr -p ${PLATFORM}
	time bash -x ./build-scripts/build-linux.sh -f android-swr -p ${PLATFORM} all
	time bash -x ./build-scripts/build-android.sh -f android-swr -p ${PLATFORM} all
	cd ${CI_PROJECT_DIR}
	time bash -x ./build-scripts/build-disk-image.sh -f android-swr -p ${PLATFORM}
	img_artifacts_path="output/${PLATFORM}/android-swr.img"
	fw_artifacts_path="output/${PLATFORM}/firmware/*.bin output/${PLATFORM}/firmware/*.zip"
else
	time bash -x ./build-scripts/fetch-tools.sh -f android-swr
	time bash -x ./build-scripts/build-linux.sh -f android-swr all
	time bash -x ./build-scripts/build-android.sh -f android-swr all
	cd ${CI_PROJECT_DIR}
	time bash -x ./build-scripts/build-disk-image.sh -f android-swr
	img_artifacts_path="output/fvp/android-swr.img"
	fw_artifacts_path="output/fvp/firmware/*.bin"
fi



# Generate Android images
${diskimg_cmd}
# Prepare files to publish
cp -a ${img_artifacts_path} ${fw_artifacts_path} ${CI_PROJECT_DIR}

# Compress the image
xz android-swr.img
# Create SHA256SUMS.txt file
sha256sum *.bin *.img.xz *.xml > SHA256SUMS.txt

# Pass variables to test job
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
echo "TC_URL=${TC_URL:-https://git.morello-project.org/morello/morello-ci-pipelines/-/jobs/artifacts/main/raw/lldb_tests.tar.xz?job=build-android}" >> ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env

#!/bin/sh

set -ex

LLVM_PROJECT_BRANCH="${LLVM_PROJECT_BRANCH:-morello/master}"

curl --connect-timeout 5 --retry 5 --retry-delay 1 -fsSLo /tmp/cheribuild.tar.bz2 \
    https://github.com/CTSRD-CHERI/cheribuild/tarball/master
mkdir -p cheribuild/src cheribuild/build cheribuild/output cheribuild/cheribsd-test-results-purecap-kernel \
    cheribuild/cheribsd-test-results-hybrid-kernel cheribuild/webkit-test-results && \
    tar -xf /tmp/cheribuild.tar.bz2 -C cheribuild --strip-components=1
ssh-keygen -t rsa -N '' -f /home/ci-runner/.ssh/cheribuild;
cat << EOF > cheribuild/cheribuild.json
{
  "source-root": "${CI_PROJECT_DIR}/cheribuild/src",
  "build-root": "${CI_PROJECT_DIR}/cheribuild/build",
  "output-root": "${CI_PROJECT_DIR}/cheribuild/output",
  "make-jobs": 4
}
EOF
echo "y" | ./cheribuild/cheribuild.py morello-qemu;
echo "y" | ./cheribuild/cheribuild.py morello-llvm \
    --morello-llvm/git-revision ${LLVM_PROJECT_BRANCH};
echo "y" | ./cheribuild/cheribuild.py cheribsd-morello-purecap;
echo "y" | ./cheribuild/cheribuild.py icu4c-native;
echo "y" | ./cheribuild/cheribuild.py icu4c-morello-purecap; 
echo "y" | ./cheribuild/cheribuild.py morello-webkit-morello-purecap \
    --morello-webkit-morello-purecap/backend tier2asm;
echo "y" | ./cheribuild/cheribuild.py disk-image-morello-purecap -f;

test_failed=0

# run webkit tests first
# Allow webkit tests to fail
echo "y" | ./cheribuild/cheribuild.py morello-webkit-morello-purecap --test \
    --test-extra-args="--junit-xml=${CI_PROJECT_DIR}/cheribuild/webkit-test-results/webkit.xml" \
    --test-ssh-key /home/ci-runner/.ssh/cheribuild.pub \
    --morello-webkit-morello-purecap/backend tier2asm || true;

# run both cheribsdtest-* and FreeBSD tests (only cat's since the full test suite is not yet passing)
# run tests against purecap kernel
echo "y" | ./cheribuild/cheribuild.py --test --test-ssh-key /home/ci-runner/.ssh/cheribuild.pub \
--test-extra-args="--test-output-dir=${CI_PROJECT_DIR}/cheribuild/cheribsd-test-results-purecap-kernel --kyua-tests-files=/usr/tests/bin/cat/Kyuafile" \
 run-morello-purecap --run/kernel-abi=purecap || test_failed=1;

# run tests against hybrid kernel
echo "y" | ./cheribuild/cheribuild.py --test --test-ssh-key /home/ci-runner/.ssh/cheribuild.pub \
--test-extra-args="--test-output-dir=${CI_PROJECT_DIR}/cheribuild/cheribsd-test-results-hybrid-kernel --kyua-tests-files=/usr/tests/bin/cat/Kyuafile" \
 run-morello-purecap --run/kernel-abi=hybrid || test_failed=1;

#alternatively we could run the above command twice one for cheribsd and one for freebsd only
#cheribsd tests only
#time ./cheribsd/repo/cheribuild.py --test --test-ssh-key /home/ci-runner/.ssh/cheribsd.pub \
#--test-extra-args="--test-output-dir=${CI_PROJECT_DIR}/cheribsd/repo/cheribsd-test-results" \
# run-morello-purecap --run/kernel-abi=purecap || test_failed=1;
#Freebsd only
#time ./cheribsd/repo/cheribuild.py --test --test-ssh-key /home/ci-runner/.ssh/cheribsd.pub \
#--test-extra-args="--test-output-dir=${CI_PROJECT_DIR}/cheribsd/repo/cheribsd-test-results --kyua-tests-files=/usr/tests/Kyuafile --no-run-cheribsdtest" \
# run-morello-purecap --run/kernel-abi=purecap || test_failed=1;

exit $test_failed

#!/bin/sh

source .gitlab-ci/build/setup-musl-build-env.sh

# Build Musl for Aarch64
${CI_PROJECT_DIR}/.gitlab-ci/build/build-toolchain-aarch64.sh $HOST_LLVM_PATH $LLVM_PROJECT $MUSL_PROJECT $SHIM_PROJECT $MORELLO_HOME $WORKSPACE

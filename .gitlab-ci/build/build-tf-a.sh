#!/bin/sh

MANIFEST_BRANCH="${MANIFEST_BRANCH:-morello/mainline}"

set -ex

rm -rf .repo/manifests
repo init --depth=1 --no-tags --no-clone-bundle \
  -u https://git.morello-project.org/morello/manifest.git \
  -b ${MANIFEST_BRANCH} -g bsp \
  --repo-rev=v2.16

repo selfupdate
repo version

# Set TF-A revision
case "$CI_PROJECT_PATH" in
  morello/trusted-firmware-a)
    TF_A_BRANCH="${TF_A_BRANCH:-morello/master}"
    xmlstarlet edit --inplace \
      --update "//project[@name='trusted-firmware-a']/@revision" \
      --value ${TF_A_BRANCH} \
      .repo/manifests/morello-base.xml
  ;;
esac

time repo sync -j8 --quiet --no-clone-bundle
repo manifest -r -o pinned-manifest.xml
cat pinned-manifest.xml

# Skip downloads
ln -sf ${HOME}/tools ${CI_PROJECT_DIR}/tools
# Get rid of checksum
sed -i "s|^    \[checksum_url\]=.*|    \[checksum_url\]=\"\"|" build-scripts/fetch-tools.sh
if ! [ -z ${PLATFORM+x} ]; then
	time bash -x ./build-scripts/fetch-tools.sh -f none -p $PLATFORM
	time bash -x ./build-scripts/build-scp.sh -f none -p $PLATFORM all
	time bash -x ./build-scripts/build-arm-tf.sh -f none -p $PLATFORM all
	time bash -x ./build-scripts/build-uefi.sh -f none -p $PLATFORM all
	time bash -x ./build-scripts/build-firmware-image.sh -f none -p $PLATFORM all
	OUT_TGT_DIR="soc"
    # Package board firmware
	curl https://git.morello-project.org/morello/board-firmware/-/archive/morello/mainline/board-firmware-morello-mainline.zip -o board-firmware.zip
	unzip board-firmware.zip
	cp ${CI_PROJECT_DIR}/output/$OUT_TGT_DIR/firmware/* board-firmware-morello-mainline/SOFTWARE/
	cd board-firmware-morello-mainline/
	zip -r ${CI_PROJECT_DIR}/output/soc/firmware/board-firmware.zip .
else
    # Build all fw besides tf-a to test ubuntu boot
    time bash -x ./build-scripts/fetch-tools.sh -f none
    time bash -x ./build-scripts/build-scp.sh -f none -p fvp all
    time bash -x ./build-scripts/build-arm-tf.sh -f none all
    time bash -x ./build-scripts/build-uefi.sh -f none all
    time bash -x ./build-scripts/build-firmware-image.sh -f none all
	OUT_TGT_DIR="fvp"
fi

cd ${CI_PROJECT_DIR}
ln output/$OUT_TGT_DIR/intermediates/morello.dtb morello.dtb
ln output/$OUT_TGT_DIR/intermediates/tf-bl1.bin tf-bl1.bin
ln output/$OUT_TGT_DIR/intermediates/tf-bl31.bin tf-bl31.bin

# Create SHA256SUMS.txt file
sha256sum morello.dtb tf-bl31.bin output/$OUT_TGT_DIR/firmware/*.bin > SHA256SUMS.txt
echo "BUILD_JOB_ID=${CI_JOB_ID}" > ${CI_PROJECT_DIR}/build.env
echo "LAVA_TEMPLATE_NAME=fvp-ubuntu.yaml" >> ${CI_PROJECT_DIR}/build.env
cat ${CI_PROJECT_DIR}/build.env

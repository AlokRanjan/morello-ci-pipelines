#!/bin/bash

project_details=$(echo ${1} | base64 --decode)
echo $project_details
echo $project_details | jq -c '.[]' | while read project_detail;
do
	PROJECT_PATH=$(echo $project_detail| jq -r ."project_path")
	SHA=$(echo $project_detail| jq -r ."sha")
	PROJECT_BRANCH=$(echo $project_detail| jq -r ."branch")
	IID=$(echo $project_detail| jq -r ."iid")
	PROJECT_NAME=$(echo ${PROJECT_PATH} | cut -d'/' -f2-)
	if [[ ${PROJECT_NAME} == "manifest" ]]; then
		echo "Found manifest project in merge requests. Checking out Branch ${PROJECT_BRANCH}"
		cd ${2}
		git fetch -a
		git checkout ${PROJECT_BRANCH}
	fi
done
